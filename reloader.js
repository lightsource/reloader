const RELOADER = {
    timeout: {
        OFFLINE: 2 * 60 * 1000, // 2 minutes
        REFRESH: 3 * 1000, // 3 seconds
    }
}

class Reload {
    constructor(cssTagSelector, jsTagSelector, timeout) {
        this.cssTagSelector = cssTagSelector
        this.jsTagSelector = jsTagSelector
        this.jsCode = ''
        this.lastMouseActionTime = 0
        this.isOnline = true
        this.statusElement = null

        'loading' === document.readyState ?
            document.addEventListener('DOMContentLoaded', this.initialize.bind(this)) :
            this.initialize()
    }

    initialize() {

        if (!document.querySelector(this.cssTagSelector)) {
            console.log('reloader : skipped. The hash is presented, but the selector is missing')
            return
        }

        this.statusElement = document.createElement('div')
        this.statusElement.setAttribute('id', 'reloader')
        this.statusElement.setAttribute('style', 'width:10px;height:10px;border-radius:50%;position:fixed;bottom:10px;left:10px;z-index:99999;background-color:green;')
        document.body.append(this.statusElement)

        this.lastMouseActionTime = new Date().getTime()
        window.addEventListener('mousemove', () => {
            this.lastMouseActionTime = new Date().getTime()
        })

        this.getJsCode(document.body).then((jsCode) => {
            this.jsCode = jsCode
            this.setReloadTimeout()
        })
    }

    isUserActive() {
        let now = new Date().getTime()

        // on mobile, the mouse actions are not available
        return (now - this.lastMouseActionTime) < RELOADER.timeout.OFFLINE ||
            screen.width < 992
    }

    getJSTag(html) {
        return html.querySelector(this.jsTagSelector)
    }

    getCSSTag(html) {
        return html.querySelector(this.cssTagSelector)
    }

    setIsOnline(isOnline) {
        if (this.isOnline === isOnline) {
            return
        }

        this.isOnline = isOnline
        this.statusElement.style.backgroundColor = this.isOnline ?
            'green' :
            'red';
    }

    setReloadTimeout() {
        if (!this.isUserActive()) {
            this.setIsOnline(false)
            setTimeout(this.setReloadTimeout.bind(this), RELOADER.timeout.REFRESH)
            return
        }

        // if just appeared, then reload without any delays
        if(!this.isOnline){
            this.setIsOnline(true)
            this.refreshRequest()
            return
        }

        setTimeout(this.refreshRequest.bind(this), RELOADER.timeout.REFRESH)
    }

    getJsCode(html) {
        let jsTag = this.getJSTag(html)

        // js tag is optional
        if(!jsTag){
            return Promise.resolve('')
        }

        let jsUrl = jsTag.getAttribute('src')

        return new Promise((resolve, reject) => {
            const request = new XMLHttpRequest()
            request.open('POST', jsUrl, true)
            request.timeout = 10000
            request.addEventListener('readystatechange', () => {
                if (4 !== request.readyState ||
                    200 !== request.status) {
                    return
                }

                resolve(request.responseText)
            })
            request.addEventListener('timeout', () => {
                console.log('reloader : js ajax failed with timeout')
                this.setReloadTimeout()
            })
            request.send()
        })
    }

    refreshRequest() {
        const request = new XMLHttpRequest()
        request.open('POST', document.location.href, true)
        request.timeout = 10000
        request.addEventListener('readystatechange', this.reload.bind(this, request))
        request.addEventListener('timeout', () => {
            console.log('reloader : refresh ajax failed with timeout')
        })
        request.send()
    }

    reloadJS(html) {
        this.getJsCode(html).then((jsCode) => {
            this.jsCode !== jsCode ?
                window.location.reload(true) :
                this.reloadCSS(html)
        })
    }

    reloadCSS(html) {
        let currentCssTag = this.getCSSTag(document)
        let newCss = this.getCSSTag(html).innerHTML
        if (currentCssTag.innerHTML !== newCss) {
            currentCssTag.innerHTML = newCss
        }

        this.setReloadTimeout()
    }

    reload(request) {
        if (4 !== request.readyState) {
            return
        }

        if (200 !== request.status) {
            console.log('reloader : fail to get new version')

            this.setReloadTimeout()
        }

        let response = document.createElement('html')
        response.innerHTML = request.responseText

        this.reloadJS(response)
    }
}

if (window.location.href.match(/#reloader/)) {
    new Reload('style#acf_blocks', 'script#acf_blocks-js')
}
