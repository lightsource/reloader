## Reloader

### What is it?

It's a sample of a Mozilla browser extension, that watches and automatically refreshes chosen inline CSS code and JS
script when
they're changed. Similar addons already exist, but some cases require custom approach (e.g. when there is inline CSS, or
when there is a dynamic builder of assets, so a whole page should be requested to get updated a JS file)

* Behind the scene it's based on ajax requests and comparing new/old versions, but this extension makes ajax of a whole
  page, it suits cases that described above
* In case when inline CSS code is changed - the CSS code will be replaced, without reloading a page
* In case when a JS script is changed - a whole page will be reloaded automatically, reloading is necessary for JS,
  because old JS code can't be unloaded, it's already executed, document events fired and objects are created)

### How to use?

1. clone the repo, update reloader.js : modify the creation line to yours  
   `new Reload('style#an_blocks', 'script#an_blocks-js', 3000)`  
   where arguments are : (cssTagSelector, jsTagSelector, timeout)
2. visit `about:debugging#/runtime/this-firefox` at your browser, click the `Load Temporary Addon` button and
   choose `manifest.json`. (Extension should be added in this way after every browser launch)
3. That's it. Now visit a target page with a `#reloader` hashtag in the url